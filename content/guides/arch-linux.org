#+TITLE: The Red Sky - Arch Linux Build Guide
#+PROPERTY: header-args :results type verbatim :exports code
#+OPTIONS: toc:nil
#+INCLUDE: ../navbar.org

#+TOC: headlines 2

* Installation

** Download Virtual Box

This can be done in any virtualization platform, but we will use virtual box for this example.

- The installer can be found at [[https://www.virtualbox.org/wiki/Downloads]]
- If you are currently running Windows, choose the "Windows hosts" option (This is not the virtual machine OS, but rather the OS you are currently using)
- Run the installer and follow the default prompts

** Download the iso

- Go to the [[https://archlinux.org/download/][Arch Linux downloads page]] and select a mirrir site under the HTTP Direct Downloads section
- Verify the SHA256 hash of the iso. As of the time of writing this, the page indicates the ISO should have a SHA256 hash of 5934a1561f33a49574ba8bf6dbbdbd18c933470de4e2f7562bec06d24f73839b

#+begin_src bash
openssl sha256 archlinux-2022.04.05-x86_64.iso
#+end_src

#+RESULTS:
: SHA256(archlinux-2022.04.05-x86_64.iso)= 5934a1561f33a49574ba8bf6dbbdbd18c933470de4e2f7562bec06d24f73839b

** Create the VM

- Open VirtualBox
- Go to File -> New Virtual Machine
- Name your system and under Type version, choose Linux, Arch Linux (64-bit)
- Click Next
- Choose a memory size of 2GB (2048 MB)
- Click Next
- Choose "Create a virtual hard disk now"
- Click Create
- Click Next
- Click Next
- In the box, type 50GB (or more)
- Click Create

** Configure the VM

- Right click on your new VM and click Settings
- Under System -> Motherboard, Enable EFI
- Under System -> Processor, set Processors to at least 2
- Under Storage -> Controller: IDE, select the disc icon
- Select the other disc icon on the right and choose to browse for your Arch Linux ISO image:

  [[../img/vbox_mount_iso.png]]

- Once you have your disk image selected, click OK
- Now we can start our VM. Select it and click "Start"

** Installation

*** Boot

- At the prompt, choose the default option to boot into the Arch Linux ISO

*** Verify Boot Mode

- Confirm that we are in EFI mode. Run the following command. If there is output, then are are in efi mode. If we get a file not found error, then we are not in efi mode and we will need to redo [[Configure the VM]].

#+begin_src bash
ls /sys/firmware/efi/efivars
#+end_src

*** Networking

- Verify network interface is recognized by the system. There should be two network interfaces: one for the loopback adapter (lo) and one for your system. Likely it will start with an 'e'. If you only have one interface, please refer to [[https://www.virtualbox.org/manual/ch06.html][The VirtualBox Networking Guide]].

#+begin_src bash
ip link
#+end_src

**** DHCP - Verify

DHCP is a protocol that allows a client machine on the same network as a DHCP server to request networking information. This information typically includes the following:

- An IP address (See [[https://en.wikipedia.org/wiki/IP_address][IP Addresses]] for more information)
- The network range that we are in (i.e. the size of our subnet. See [[https://en.wikipedia.org/wiki/Subnetwork][Subnetwork]] for more information)
- The DNS servers for the network (see [[https://en.wikipedia.org/wiki/Domain_Name_System][Domain Name System]] for more information)

This protocol allows machines to setup their own networking, rather than a person having to do it themselves.

In this case, we use the ip addr command to make sure that we were given an IP addres. As seen below, our network interface "enp3s0" was given an IP of "172.16.1.220" and a network of "/24". 

#+begin_src bash
ip addr
#+end_src

#+RESULTS:
#+begin_example
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 00:d8:61:88:d0:f9 brd ff:ff:ff:ff:ff:ff
    inet 172.16.1.220/24 brd 172.16.1.255 scope global dynamic noprefixroute enp3s0
       valid_lft 549999sec preferred_lft 549999sec
    inet6 fe80::a8b2:4d12:7e52:4866/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: wlo1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 7c:b2:7d:fc:46:3f brd ff:ff:ff:ff:ff:ff
    altname wlp0s20f3
    inet 172.16.1.215/24 brd 172.16.1.255 scope global dynamic noprefixroute wlo1
       valid_lft 559871sec preferred_lft 559871sec
    inet6 fe80::ebc9:b83b:12a0:9031/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
#+end_example

**** DNS Verify

Linux stores its DNS server configuration in "/etc/resolv.conf". We will use the cat command to view the contents of this file. You should see at least one "nameserver <ip>" line in this file. If not, DNS was not configured properly.

#+begin_src bash
cat /etc/resolv.conf
#+end_src


**** Internet Connectivity Check

Here we will use the ping command to verify internet connectivity. See [[https://en.wikipedia.org/wiki/Ping_(networking_utility)][Ping]] for more information. Alternatively, you can use the "man" command to get information about ping ("man ping"). 

#+begin_src bash
ping archlinux.org
#+end_src

*** Disk Setup

- We are going to need three partitions:
  1. EFI Boot partition
  2. The root partition
  3. The swap partition

**** Ensure Disk is Recognized by the system

- Run the lsblk utility and look for an entry that says "disk" under the "Type" heading

#+begin_src bash
lsblk
#+end_src

#+RESULTS:
: NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
: sda           8:0    0 953.9G  0 disk
: nvme0n1     259:0    0 476.9G  0 disk
: ├─nvme0n1p1 259:1    0   300M  0 part /boot/efi
: └─nvme0n1p2 259:2    0 476.6G  0 part /

- Take note of the "NAME"

**** Create Partition Table

- Next, we are going to start the parted utility to edit our disk

#+begin_src bash :exports code
parted /dev/{device_name} # In my case, it is "sda"
#+end_src

- Now we will use the mklabel command to create a partition table. We are going to use the gpt format

#+begin_src bash :exports code
mklabel gpt
#+end_src

**** EFI Boot Partition

- To create the boot partition, we need to know where the first available byte is on our disk. We will use the "print free" command to determine this:

#+begin_src bash :exports code
print free
#+end_src

- We will use the "Start" as the beginning of our partition and then add the number of bytes we want onto that.

#+begin_src bash :exports code
mkpart gpt fat32 {start} 500MB
#+end_src

- If you are prompted about the "closest" the system can get, just type yes and hit enter
- If you are prompted again about the system alignment, just hit I and enter to ignore

- Next we are going to set the esp (boot) flag on our partition. Use the "Print" command to get the partition number of your drive and then run:

#+begin_src bash :exports code
set {partition_number} esp on
#+end_src

**** Swap Partition

- For the swap partition, we will use the mkpart utility again. Make sure you find your "start" using the print utility first.

#+begin_src bash :exports code
mkpart gpt ext4 {Start} {End}
#+end_src

- Now we will exit parted with the quit command and set our swap partition to be recognized as swap
- To do this, we will need to know the partition name of the swap partition using lsblk. We know our swap partition should be the second partition and should be about 1GB in size

#+begin_src bash :exports code
lsblk
#+end_src

- We will use the mkswap command to enable swap on our partition:

#+begin_src bash :exports code
mkswap /dev/{swap_partition_name}
#+end_src

**** Root Partition

- Open parted back up on your disk:

#+begin_src bash :exports code
parted /dev/{disk_name}
#+end_src-

- We will make one more partition using the mkpart command. This time, we will use the rest of the disk. We will make life easy and just use "100%" for the end. Parted will know this means the rest of the disk

#+begin_src bash :exports code
mkpart gpt ext4 {Start} 100%
#+end_src

**** File System

Now that we have our partitions created, we need to add a file system to it.

Our root partition file system type was specified as ext4 and our boot partition was fat32.

- We will use the lsblk command again to get our partition names. Roots will be the last one, while the boot partition will be first, if you followed the steps in the same order as they were done above.

#+begin_src bash
lsblk
#+end_src

#+RESULTS:
: NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
: sda           8:0    0 953.9G  0 disk
: nvme0n1     259:0    0 476.9G  0 disk
: ├─nvme0n1p1 259:1    0   300M  0 part /boot/efi
: └─nvme0n1p2 259:2    0 476.6G  0 part /

- To create the ext4 file system, we will use the mkfs.ext4 utility and to create the fat32 file system, we will use the mkfs.fat utility with the -F flag set to 32:

#+begin_src bash :exports code
mkfs.ext4 /dev/{root_partition_name}
mkfs.fat -F 32 /dev/{boot_partition_name}
#+end_src

**** Mounting

Now that we have the root partition file system created, it is ready to be written to. The problem is, however, that we can't access the disks, yet. First, we have to mount the partition to our temporary iso file system. This will add a folder to our file system that maps to the partition.

- To do this, we will use the mnt command, but first, we have to create somewhere to mount it to. While you can mount it to any directory on the system, the /mnt directory exists for this purpose.
- First, we will create a folder in /mnt called arch (this can be called anything, it doesn't matter)

#+begin_src bash :exports code
mkdir /mnt/arch
#+end_src

- Now mount the drive using the mount command

#+begin_src bash :exports code
mount /dev/{root_partition_name} /mnt/arch
#+end_src

- We also need to mount our boot partition. This partition goes in the {root_fs_mount_point}/boot.

#+begin_src bash :exports code
mkdir /mnt/arch/boot
mount /dev/{boot_partition_name} /mnt/arch/boot
#+end_src

- Since we are getting our disks ready to work on, let's also enable the swap partition. I will explain why we do this later

#+begin_src bash :exports code
swapon /dev/{swap_partition_name}
#+end_src

*** OS Installation

**** Package Management

In order to download packages, we need to tell our package manager (pacman) where to download them from. The file that holds the list of servers to download from is /etc/pacman.d/mirrorlist.

We won't be editing this by hand, however. There is a nice utility called reflector that we will use to choose our servers for us based on our preferences.

- We will use the --country flag to tell reflector we want to use US-based mirrors (servers that hold packages) and the --protocol flag to specify that we only want to download over https
- We will redirect the output of that command into the mirrorlist file. If you're curious, you can run it without the redirect to see what it outputs. This will not effect your system

#+begin_src bash :exports code
reflector --country US --protocol https > /etc/pacman.d/mirrorlist
#+end_src

**** Kernel Install

Now it's time to install Linux onto the drive. We do this usin the pacstrap command. When we are booted into the system, we will use pacman to install packages. However, since we are still booted into our iso, we will use the pacstrap command to install packages into the mounted file system. This command will install three packages:

- base - the arch linux base package containing necessary arch packages
- linux - the linux kernel
- linux-firmware - software needed for linux to interface with the firmware

#+begin_src bash :exports code
pacstrap /mnt/arch base linux linux-firmware
#+end_src

**** Networking

To manage the networking on the system, we will need a network manager. There are a few to choose from, but we are going to choose "NetworkManager", which is a network manager that has a GUI that we can use later if we want to.

#+begin_src bash :exports code
pacstrap /mnt/arch networkmanager
#+end_src

**** Text Editing

We need to install a text editor so we can edit files. For simplicity, we will install both nano and vim.

#+begin_src bash :exports code
pacstrap /mnt/arch nano vim
#+end_src

**** Documentation

We will also need to install man pages for package documentation.

#+begin_src bash :exports code
pacstrap /mnt/arch man-db man-pages
#+end_src

*** OS Configuration

Now that the operating system is installed, we need to configure the various parts of the system.

**** Fstab

To ensure our disks are re-mounted on boot, we need to populate the /etc/fstab file in our system. There is a nice utility to do this for us called genfstab, copying the current mounting setup as we have it in our iso environment (Remember, we mounted the drives earlier so we could edit them).

- Run the genfstab command, pointing it at our mounted file system

#+begin_src bash :exports code
genfstab -U /mnt/arch >> /mnt/arch/etc/fstab
#+end_src

- Verify fstab was populated properly with cat:

#+begin_src bash :exports code
cat /mnt/arch/etc/fstab
#+end_src

**** Chroot

The next few configurations will require our tools to think we are booted into our drives, even though we are booted into the iso system. To do this we will use the chroot utility. Actually, we will use Arch linux's version of the chroot tool, called arch-chroot.

- Call the chroot utility and point it at our mounted drive:

#+begin_src bash :exports code
arch-chroot /mnt/arch
#+end_src

Everything you do from this point on will be done inside your newly installed arch system.

**** Timezone

The next thing we will do is set the timezone. Let's see what our timezone is set to right now with the timedatectl command:

#+begin_src bash
timedatectl status
#+end_src

#+RESULTS:
:                Local time: Sat 2022-04-23 17:30:52 EDT
:            Universal time: Sat 2022-04-23 21:30:52 UTC
:                  RTC time: Sat 2022-04-23 21:30:52
:                 Time zone: America/New_York (EDT, -0400)
: System clock synchronized: yes
:               NTP service: inactive
:           RTC in local TZ: no

Most likely, your system is currently running in the UTC timezone. To change it, we first need to gather a list of time zones:

#+begin_src bash :exports code
timedatectl list-timezones
#+end_src

Set the timezone to your current timezone. We will do this by creating a "soft link" between our selected time zone and our system's localtime file.

#+begin_src bash :exports code
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
#+end_src

Run the hwclock utility to generate an /etc/adjtime file. This file keeps track of the clock's drift, allowing the clock to adjust the system time as needed when it gets off track.

#+begin_src bash :exports code
hwclock --systohc
#+end_src

Let's verify the system time zone was change successfully with the date command:

#+begin_src bash
date
#+end_src

#+RESULTS:
: Sat Apr 23 05:43:06 PM EDT 2022

**** Localization

Localization is used to specify the region language and format. For us, this is en_US.UTF-8. en_us being english, US and UTF-8 being the text encoding format that we use.

- Edit the /etc/locale.gen file and uncomment it.

#+begin_src bash :exports code
vim/nano /etc/locale.gen
#+end_src

- We also need to create the file /etc/locale.conf and add the following line:

#+begin_src text :exports code
LANG=en_US.UTF-8
#+end_src

- Now we will run the local-gen command to finish it up

#+begin_src shell
locale-gen
#+end_src

**** Network Configuration

The only thing we need to do for networking is to set the system hostname. This is the "Name" of the system, as other entities on the network will see it.

- To do this, edit the /etc/hostname file and simply add your hostname to it (nothing else)

#+begin_src shell
vim/nano /etc/hostname
#+end_src

**** Root Password

Set the root password with the passwd utility:

#+begin_src bash :exports code
passwd
#+end_src

*** Boot Loader

Now that our operating system is configured and ready to run, we will install a boot loader. The boot loader is the system that the BIOS calls when the system is turned on. The boot loader will then set the necessary parameters and start the kernel.

The vast majority of linux systems are using GRUB (GRand Unified Bootloader). We will also, as it will be the most supported and documented.

Now that we have tricked our tools into thinking that we are booted into the system drive, we will use the system's package manager (pacman) to install packages.

Additionally, we need to install microcode updates from Intel/AMD (depending on your CPU). If you don't have an Intel or AMD CPU, skip this part. Microcode updates install bug fixes in the microcode of the processor. For more details, see the [[https://wiki.archlinux.org/title/Microcode][Arch Wiki "Mirocode" section]]. For Intel, this package is called intel-ucode. For AMD, amd-ucode.

- Using pacman, install the grub and efibootmgr packages using the -S flag

#+begin_src bash :exports code
pacman -S grub efibootmgr intel-ucode/amd-ucode
#+end_src

Now we will install GRUB into our boot partition. Remember where our boot partition is mounted? While outside of our file system (not chrooted), we mounted it to /mnt/arch/boot. Now that we have chrooted into /mnt/arch, it is now just mounted at /boot.

- Install GRUB into our boot partition with the grub-install command:

#+begin_src bash :exports code
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
#+end_src

- Finally, generate the GRUB configuration file, with grub-mkconfig

#+begin_src bash :exports code
grub-mkconfig -o /boot/grub/grub.cfg
#+end_src

*** REBOOT

Congratulations, you have now installed your Arch Linux operating system. We need to reboot and make sure it worked. Let's exit our chroot (with exit) and unmount the partitions with the umount command:

#+begin_src bash :exports code
exit
umount -R /mnt/arch
#+end_src

- Now let's reboot with the reboot command

#+begin_src bash :exports code
reboot
#+end_src

Note: If we were not using a virtual machine, this is when we would remove our install media (flash drive, CD, etc.) that we booted from to keep the system from booting into it again. Virtual Box will do this for us, though.

* Post Installation

Now that our system is installed properly, there are some post-installation tasks that will need to be performed.

** Networking

We installed the NetworkManager package to manage networking. Let's use the systemctl utility to see if it's running

#+begin_src bash :exports code
systemctl status NetworkManager # THIS IS CASE SENSITIVE
#+end_src

You will notice that the service is not running. This is because we never enabled it. We can do that with the systemctl utility again.

- Enable and start the NetworkManager service

#+begin_src bash :exports code
systemctl enable NetworkManager
systemctl start NetworkManager
#+end_src

- If you need to connect to a wireless network, use:

#+begin_src shell
nmtui
#+end_src

- Now let's see if we have an IP address:

#+begin_src bash
ip addr
#+end_src

#+RESULTS:
#+begin_example
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 00:d8:61:88:d0:f9 brd ff:ff:ff:ff:ff:ff
    inet 172.16.1.220/24 brd 172.16.1.255 scope global dynamic noprefixroute enp3s0
       valid_lft 519858sec preferred_lft 519858sec
    inet6 fe80::a8b2:4d12:7e52:4866/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: wlo1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 7c:b2:7d:fc:46:3f brd ff:ff:ff:ff:ff:ff
    altname wlp0s20f3
    inet 172.16.1.215/24 brd 172.16.1.255 scope global dynamic noprefixroute wlo1
       valid_lft 585201sec preferred_lft 585201sec
    inet6 fe80::ebc9:b83b:12a0:9031/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
#+end_example

- Just to be thorough, let's also make sure we have internet connectivity with the ping command, like we did in our live iso environment:

#+begin_src bash :exports code
ping -c 3 archlinux.org
#+end_src

#+RESULTS:
: PING archlinux.org (95.217.163.246) 56(84) bytes of data.
: 64 bytes from archlinux.org (95.217.163.246): icmp_seq=1 ttl=44 time=131 ms
: 64 bytes from archlinux.org (95.217.163.246): icmp_seq=2 ttl=44 time=132 ms
: 64 bytes from archlinux.org (95.217.163.246): icmp_seq=3 ttl=44 time=127 ms
:
: --- archlinux.org ping statistics ---
: 3 packets transmitted, 3 received, 0% packet loss, time 2003ms
: rtt min/avg/max/mdev = 127.281/130.018/132.270/2.065 ms

** Users and Groups

Currently we are running our system as the root user. The root user has 100% full privileges on the system. This is very dangerous, not just for security purposes, but for system safety. If we run the wrong command as root, we can brick our system very quickly.

- Let's add a non-root user for us to login as with the useradd utility.

--create-home :  setup the user's home directory in /home (like C:\Users in Windows).
-s : set user's default shell

#+begin_src bash :exports code
useradd --create-home -s /bin/bash {username}
#+end_src

- Let's also create a password for our user:

#+begin_src bash :exports code
passwd {username}
#+end_src

- This is our system, so we will still want the ability to run privileged commands. Without it, we can't do much on the system. To do this, we will use the "sudo" package. This is not installed by default.

#+begin_src bash :exports code
pacman -S sudo
#+end_src

Now that we have sudo installed, we need to tell the system that our user has privileges to use it. To do this we need to edit the /etc/sudoers file. HOWEVER: we will not be manually editing this file. This is very dangerous and can mess up your system if you lose access to the root user. To edit this command, we will use the visudo utility.

- You will also need to tell visudo what text editor to use when editing the file. We do this by setting the EDITOR variable before running the command. We will make this change permanent later.

#+begin_src bash :exports code
EDITOR=vim/nano visudo
#+end_src

-  Uncomment the following line:

#+begin_src text :exports code
%sudo ALL=(ALL:ALL) ALL
#+end_src

- Optionally, you can tweak the line to not require a password by adding NOPASSWD:

#+begin_src text :exports code
%sudo ALL=(ALL:ALL) NOPASSWD: ALL
#+end_src

This will grant any user that is a member of the "sudo" group sudo privileges.

- The sudo group does not exist yet. We can create it with the groupadd utility:

#+begin_src bash :exports code
groupadd sudo
#+end_src

- We are not in the sudo group yet, so let's add ourselves with the usermod (user modification) utility:

#+begin_src bash :exports code
usermod -aG sudo {username}
#+end_src

- Let's change to our user with the su (switch user) command:

#+begin_src bash :exports code
su {username}
#+end_src

- To use sudo, simply prefix your command with "sudo". Let's see if our changes worked by listing the contents of the /root directory:

#+begin_src bash :exports code
sudo ls /root
#+end_src

If there was non-error output or if there was no output, this is working as expected.

** Shell Customization

Now that we have our user set up, let's make life a little easier by automating some tasks

*** .bashrc

Since we are using the bash shell, we can run commands any time we open a new shell by editing the .bashrc file.

This file is located in the user's home directory. The current home directory can be shorthanded with '~'. For instance, ~/.bashrc is the same as /home/{username}/.bashrc

- Let's create this file. First, we need to change to our home directory. Let's do this with the cd command:

#+begin_src bash :exports code
cd ~/
#+end_src

- Now, let's edit our .bashrc file

#+begin_src bash :exports code
vim/nano .bashrc
#+end_src

There may be some code in there already. This is fine and we are going to append our changes to the end of the file.

*** EDITOR

Earlier we had to tell visudo what our default text editor was. Let's make this a permanent change using the export command in our .bashrc file. Also, some programs use the "VISUAL" environment variable, so we will set that as well

Get in the habit of adding comments to everything that you do. Nothing should ever be configured without comments if you can help it.

#+begin_src bash :exports code

# default text editor
export EDITOR=vim/nano
export VISUAL=vim/nano

#+end_src

*** Aliases

There will be some common commands that you use all the time that get tiring to type over and over. Let's create aliases for some commands we will be running frequently:

#+begin_src bash :exports code

  # -l : show details
  # -a : show hidden files
  # -h : show file sizes in human readable format
  alias ll="ls -lah"
  alias l="ls -lh"

  # cd N directories up
  alias ..="cd .."
  alias ...="cd ../.."
  alias ....="cd ../../.."

  # -S : install
  # -y : update package index from repositories
  alias install="sudo pacman -Sy"

  # -u : upgrade packages
  alias upgrade="sudo pacman -Syu"

  # -r : remove package
  alias remove="sudo pacman -R"

  # text editor alias
  alias edit="$EDITOR"

  # edit as root
  alias sedit="sudo $EDITOR"

  # redo last command as root
  alias please='sudo "$BASH" -c "$(history -p !!)"'

#+end_src

Now we will tell our current shell environment to use these settings:

#+begin_src shell
source .bashrc
#+end_src

** Common User Directories

In Windows, we are used to having some common user directories, such as "Desktop" or "Documents". Let's get that same experience in Linux.

- We could just create them with mkdir, but for getting some external functionality that we will use later, let's install [[https://wiki.archlinux.org/title/XDG_user_directories][XDG User Directories]].

#+begin_src bash :exports code
install xdg-user-dirs # We use our install alias that we created earlier
#+end_src

- Now we can run xdg-user-dirs-update to create the directories

#+begin_src bash :exports code
xdg-user-dirs-update
#+end_src

- Now we should see our new user folders in our home directory

#+begin_src bash :exports code
ls ~
#+end_src

* GUI Interface

** Concepts
*** Display Manager (Login Manager)

The display manager is the first thing we see when we boot up our operating system. The display manager's job is to handle our login process as well as start the display server and window manager

*** Display Server

The display server is the manager of all graphical applications. Any application that runs with a GUI is a client of the display server. This server will handle inputs from the kernel and send them to the correct GUI client. Your keyboard, for example, will be sent to the currently focused window. 

The most popular display server protocol is the X Window System and the main program that implements this is call Xorg. 

A newer alternative to the X Window System (although still not extremely popular) is Wayland.

*** Window Manager

The window manager is responsible for handling windows on the screen. Where to place them, how to show and interact with them, etc. A window is a single instance of a program shown in a single pane on the screen.

Typically, the window manager is also responsible for handling the panel, if it has one. This is the bar at one of the edges of the screen that present you with information about currently opened windows, the date/time, etc.

*** Desktop Environment

A desktop environment is a Window Manager packaged with many other applications that help handle the system. For example, a settings menu, system tray, task manager, etc.

These come in a lot of different shapes and sizes, but make your life much easier when using the system. These are not required and a simple standalone Window Manager can be used. However, for beginners, this is not recommended, as you will be missing a lot of things that you may assume are fundamental to the operating system, such as a settings application.

Some DE's also ship with a display manager, such as Gnome, whch ships with the Gnome Display Manager (GDM). 

** Installation

Now we are going to install a user interface into our system. This is where you really get to branch off into making your system your own. We are going to install a few things:

- (xorg-server, xorg-xinit xorg-xrdb) A display server. This display server will be responsible for handling outputting content to our screen.
- (lightdm lightdm-gtk-greeter) A display manager (or login manager). This is what will handle user authentication and will start the display server.
- (cinnamon) - A windows-like desktop environment (KDE Plasma is another good alternative here)
- (cutefish) - A mac-like desktop environment
- (alacritty) A terminal emulator. This will give us access to the cli (like we are using right now) inside of a window. In windows, this is the only way to get to the cli, so it should be familiar if you have ever used them.
- (ttf-dejavu) A system font. Without fonts downloaded on your system, the display manager can't show you text. We will install the DejaVu font, as it is pretty easy on the eyes. 

#+begin_src shell

install xorg-server xorg-xinit xorg-xrdb lightdm lightdm-gtk-greeter cinnamon cutefish alacritty ttf-dejavu 

#+end_src

Our display server will run as a service and will be the first thing we see when our OS boots. We will need to enable this service. 

#+begin_src shell

sudo systemctl enable lightdm

#+end_src

** Arch User Repository

#+begin_src shell

	pacman -S --needed git base-devel
	git clone https://aur.archlinux.org/yay.git
	cd yay
	makepkg -si

#+end_src 

** Fix Icons

#+begin_src shell

  pacman -S hicolor-icon-theme
  pacman -S adwaita-icon-theme
  yay -S nerd-fonts-sf-mono

#+end_src 

** Reboot

 The Login Manager is going to take ahold of our session on the next boot. To get into the Login Manager, we will reboot.

#+begin_src shell
sudo reboot
#+end_src

** First Steps


*** Fix Screen Size

Now that we have the terminal open, let's fix the resolution of the screen using the xrandr command

#+begin_src shell
xrandr -s widthxheight
#+end_src

*** Install Virtual Box Tools

VirtualBox has some tools used to manage the system. We will install this next:

#+begin_src shell
install virtualbox-guest-utils
#+end_src

We will also need to enable the VirtualBox service

#+begin_src shell
sudo systemctl enable vboxservice
#+end_src

This will require a reboot. Also, in Virtual Box, open the VM settings. Under "General" -> "Advanced" enable clipboard and drag 'n drop features. 

* Conclusion

At this point, you have a functional operating system with some minor customizations. Future tutorials will branch into very different categories. It is highly recommended that you zip this VM up or clone it. I recommend zipping, as it will take up less space and will be untouchable in VirtualBox, keeping you from accidentally messing with it in the application.

This build is incredibly minimal, however, you could do some very basic work on this system or install what you need and run it as a server. Below is my htop output from this machine. As you can see, very few processes are running on the system and it is only using 105 MB of memory. Contrast that with an operating system like Windows, which uses upwards of 2-3 GB cold boot (i.e., nothing extra running).

[[../img/htop_output.png]]

* Appendix

** Base Package Contents

#+begin_src text :exports code
bash
bzip2
coreutils
file
filesystem
findutils
gawk
gcc-libs
gettext
glibc
grep
gzip
iproute2
iputils
licenses
pacman
pciutils
procps-ng
psmisc
sed
shadow
systemd
systemd-sysvcompat
tar
util-linux
xz
#+end_src

** [archived] Customization

For the next few steps, we are going to need to clone this git repository (the one that generates this web site). Let's install git

#+begin_src shell
install git
#+end_src

Now, run the following commands

#+begin_src shell
git clone https://gitlab.com/t9741/arch-linux-guide.git
cd arch-linux-guide
#+end_src

This will download some configuration files that we are going to use as a base line for our build.

*** QTile

QTile (or window manager), has the most impact on the look/feel of our system. It's the first thing we see when we log in and the QTile panel is always there. Let's start by copying over our base config to it:

#+begin_src shell
cp content/configs/tangled/qtile/.config/qtile/config.py ~/.config/qtile/config.py
#+end_src

To make our QTile config work we will need to run the following commands:

#+begin_src shell
  install python-pip
  pip install psutil
  touch ~/.config/qtile/modkey
  touch ~/.config/qtile/autostart.sh
  chmod +x ~/.config/qtile/autostart.sh
#+end_src

Typically, after changing our QTile config, we won't need to reboot. However this time we will to make the psutil change take effect. This won't need to be done again.

Now, our QTile config should look much better than it did before.

To read more about this configuration, see [[../configs/qtile.org][My Qtile Configuration]]. 

*** DMenu

Let's also replace the run launcher with a better program called dmenu.

#+begin_src shell
install dmenu
#+end_src

There is a keybinding in QTile to run this program (mod+shift+D). 

*** Alacritty

The next most common program we will see on our screen is alacritty. Let's copy the config over for this as well.

#+begin_src shell
mkdir -p ~/.config/alacritty
cd ~/arch-linux-guide
cp content/configs/tangled/alacritty/.config/alacritty/alacritty.yml ~/.config/alacritty/
#+end_src

You can edit the "~/.config/alacritty.yml" file and change the "size: 9" line to increase the font size. I recommend 14-18. Alacritty should update as soon as you write the changes. 

See [[../configs/alacritty.org][My Alacritty Config]].

*** Wallpaper

Some wallpapers would make this look much better. To handle our wallpapers, we will use a program called "nitrogen".

#+begin_src shell
install nitrogen
#+end_src

Nitrogen itself does not have wallpapers, so we will download some. 

#+begin_src shell
install archlinux-wallpaper
#+end_src

Now let's use dmenu to open it. Use the mod+shift+D key binding to open dmenu and run "nitrogen".

Once open, we will go to "preferences" and add a new folder for wallpapers. Add "/usr/share/backgrounds" to the wallpapers folders. 

[[../img/nitrogen.png]]

Change "Full Screen" to "Screen 1", select a wallpaper and hit "Apply"

[[../img/nitrogen_apply.png]]
