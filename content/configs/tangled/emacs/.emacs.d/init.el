(setq inhibit-startup-message t)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(set-fringe-mode 10)
(menu-bar-mode -1)

;; Set font
(set-face-attribute 'default nil :font "DejaVu Sans Mono" :height 150)

(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("melpa-stable" . "https://stable.melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))
(package-initialize)
(unless package-archive-contents 
  (package-refresh-contents))
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-one t)
  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  ;; Enable custom neotree theme (all-the-icons must be installed!)
  (doom-themes-neotree-config)
  ;; or for treemacs users
  (setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
  (doom-themes-treemacs-config)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))

(use-package evil
  :ensure t
  :init
  (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1))

(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))
(setq evil-collection-setup-minibuffer t)

(use-package which-key
  :init
  (which-key-mode)
  :config
  (require 'which-key)
  (which-key-setup-minibuffer)
  (setq which-key-idle-delay 0.3))

(use-package general
  :config
  (require 'general))
(general-create-definer leader-key
			:prefix "SPC")
;; General Keybindings

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))

(use-package ivy
  :diminish
  :bind (:map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config)
(ivy-mode 1)

(use-package ivy-rich
  :config
  (ivy-rich-mode 1))

(use-package tabspaces
  :hook (after-init . tabspaces-mode)
  :commands (tabspaces-create-workspace
             tabspaces-create-new-project-and-workspace
             tabspaces-open-existing-project-and-workspace
             tabspaces-switch-workspace)
  :custom
  (tabspaces-use-filtered-buffers-as-default t)
  (tabspaces-default-tab "Default")
  (tabspaces-remove-to-default t)
  (tabspaces-include-buffers '("*scratch*"))
  (tab-bar-show nil))

(require 'recentf)
(recentf-mode 1)

(column-number-mode)
(global-display-line-numbers-mode t)

(dolist (mode '(org-mode-hook
                term-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  (when (file-directory-p "~/Projects/Code")
    (setq projectile-project-search-path '("~/Projects/Code")))
  (setq projectile-switch-project-action #'projectile-dired))

  ;; Counsel Integration
(use-package counsel-projectile
 :after projectile
 :config
 (counsel-projectile-mode 1))

(use-package magit
  :commands (magit-status magit-get-current-branch)
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))
(use-package forge)

(defun dw/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (auto-fill-mode 0)
  (visual-line-mode 1)
  (setq evil-auto-indent nil))

(use-package org
  :hook (org-mode . dw/org-mode-setup)
  :config
  (setq org-ellipsis " ▾"
        org-hide-emphasis-markers t))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

;; Replace list hyphen with dot
(font-lock-add-keywords 'org-mode
                        '(("^ *\\([-]\\) "
                          (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))


;; Make sure org-indent face is available
(require 'org-indent)

;; Ensure that anything that should be fixed-pitch in Org files appears that way
(set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
(set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-indent nil :inherit '(org-hide fixed-pitch))
(set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)
(set-face-attribute 'org-table nil :font "DejaVu Sans Mono")

;; Centering Text
(defun efs/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
	visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . efs/org-mode-visual-fill))

;; Babel
(org-babel-do-load-languages
  'org-babel-load-languages
  '((emacs-lisp . t)
    (python . t)))

(setq org-confirm-babel-evaluate nil)

;; This is needed as of Org 9.2
(require 'org-tempo)

(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))

(defun efs/lsp-mode-setup()
  (setq lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
  (lsp-headerline-breadcrumb-mode))

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :hook (lsp-mode . efs/lsp-mode-setup)
  :config
  (lsp-enable-which-key-integration))

(use-package lsp-ui
:hook (lsp-mode . lsp-ui-mode))

(setq lsp-ui-doc-position 'bottom)
(setq lsp-ui-sideline-enable nil)
(setq lsp-ui-sideline-show-hover nil)

(use-package lsp-treemacs
:after lsp)

(use-package lsp-ivy)

(use-package company
  :after lsp-mode
  :hook (prog-mode . company-mode)
  :bind (:map company-active-map
         ("<tab>" . company-complete-selection))
        (:map lsp-mode-map
         ("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))

(use-package company-box
  :hook (company-mode . company-box-mode))

(use-package python-mode
  :hook (python-mode . lsp-deferred)
  :config
  :custom
  (setq python-shell-interpreter "/opt/anaconda/bin/python"))

(setenv "PATH" (concat "/opt/anaconda/bin:" (getenv "PATH") ))

(use-package simple-httpd
  :ensure t)

(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "SPC"
 "w h" 'evil-window-left
 "w l" 'evil-window-right
 "w j" 'evil-window-down
 "w k" 'evil-window-up
 "w c" 'evil-window-delete
 "w w" 'evil-window-prev
 "w e" 'evil-window-next
 "w r" 'revert-buffer)

(evil-global-set-key 'normal
   (kbd "C-l") 'evil-window-increase-width)
(evil-global-set-key 'normal
   (kbd "C-h") 'evil-window-decrease-width)
(evil-global-set-key 'normal
   (kbd "C-k") 'evil-window-increase-height)
(evil-global-set-key 'normal
   (kbd "C-j") 'evil-window-decrease-height)

(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "SPC"
 "h f" 'counsel-describe-function
 "h v" 'counsel-describe-variable
 "h k" 'general-describe-keybindings
 "h m" 'describe-mode)

(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "SPC"
 "TAB n" 'tabspaces-create-workspace
 "TAB c" 'tabspaces-close-workspace
 "TAB 1" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "1")) :which-key "Workspace: 1")
 "TAB 2" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "2")) :which-key "Workspace: 2")
 "TAB 3" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "3")) :which-key "Workspace: 3")
 "TAB 4" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "4")) :which-key "Workspace: 4")
 "TAB 5" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "5")) :which-key "Workspace: 5")
 "TAB 6" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "6")) :which-key "Workspace: 6")
 "TAB 7" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "7")) :which-key "Workspace: 7")
 "TAB 8" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "8")) :which-key "Workspace: 8")
 "TAB 9" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "9")) :which-key "Workspace: 9")
 "TAB 0" '((lambda() (interactive) (tabspaces-switch-to-or-create-workspace "0")) :which-key "Workspace: 0"))

(general-define-key
  :states 'normal
  :keymaps 'override
  :prefix "SPC"
  "b b" 'buffer-menu
   "`" 'switch-to-prev-buffer)

(general-define-key
 :states 'normal
  :keymaps 'override
 :prefix "SPC"
 ":" 'counsel-M-x
 "d e b" 'eval-buffer)

(general-define-key
 :states 'normal
  :keymaps 'override
 :prefix "SPC"
 "." 'counsel-find-file
 "n h" '((lambda() (interactive) (dired-at-point "~/")) :which-key "File Browse: Home")
 "n p" '((lambda() (interactive) (dired-at-point "~/projects")) :which-key "File Browse: Projects")
 "n l" '((lambda() (interactive) (dired-at-point "~/linux-scripts")) :which-key "File Browse: Linux Scripts & Configs")
 "n c" '((lambda() (interactive) (dired-at-point "~/.config")) :which-key "File Browse: XDG Configs Directory")
 "n r" 'counsel-recentf)

(evil-collection-define-key 'normal 'dired-mode-map
 "h" 'dired-up-directory
 "l" 'dired-find-file)

(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "SPC"
 "m m" 'magit)

(general-define-key
 :states 'normal
 :keymaps 'override
 :prefix "SPC"
 "d o p" 'run-python
 "d e p" 'python-shell-send-buffer
 "d l f" 'flymake-mode)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(forge evil-magit magit counsel-projectile projectile doom-themes ivy-rich rainbow-delimiters ivy org-babel org-mode general which-key use-package evil-collection)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
