from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy, LazyCommandInterface
from libqtile.utils import guess_terminal

import os
import subprocess

PRIMARY_COLOR = ['#323945', '#323945'] 

MOD_KEYS = {
    'mod1': 'Alt',
    'mod4': 'Super',
    'shift': 'Shift',
    'control': 'Control'
}

MOD_KEY_FILE = os.path.expanduser('~/.config/qtile/modkey')

keys = []

def get_mod():
    with open(MOD_KEY_FILE, 'r') as f:
        return f.read().strip() or 'mod4'


mod = get_mod()

groups = [Group(i) for i in "1234567890"]

default_layout_config = {
    "border_focus": "#848cab",
    "border_width": 2,
    "margin": 8
}

layouts = [
    #layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4),
    #layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    layout.MonadTall(**default_layout_config),
    layout.MonadWide(**default_layout_config),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)

layouts.append(floating_layout)

widget_defaults = {
    'font': "DejaVuSansMono",
    'fontsize': 16,
    'background': PRIMARY_COLOR
}

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayout(**widget_defaults),
                widget.Sep(linewidth=2),
                widget.GroupBox(**widget_defaults, active=["#949494", "#949494"], inactive=["#ffffff", "#ffffff"]),
                widget.Sep(linewidth=2),
                widget.WindowName(**widget_defaults, format="{name}"),
                widget.TextBox(**widget_defaults, text=f'{MOD_KEYS.get(mod)}'),
                widget.Sep(linewidth=2),
                widget.Memory(**widget_defaults, format='Memory: {MemPercent}%'),
                widget.CPU(**widget_defaults, format='CPU: {load_percent}%'),
                widget.Sep(linewidth=2),
                widget.Spacer(length=5),
                widget.BatteryIcon(**widget_defaults, battery="/sys/class/power_supply/BAT0", update_interval=1),
                widget.Battery(**widget_defaults, battery="/sys/class/power_supply/BAT0", update_interval=15, format="{percent:2.0%}"),
                widget.Sep(linewidth=2),
                widget.Clock(**widget_defaults, format="%Y-%m-%d %a %H:%M:%S"),
                widget.Systray(**widget_defaults),
            ],
            32,
        ),
    ),
]

keys.extend([
    Key([mod], "r", lazy.spawn(os.path.expanduser('~/.scripts/toggle_qtile_mod.py')), lazy.reload_config())
]),

keys.append(Key([mod], "Return", lazy.spawn('alacritty')))

keys.extend([
    Key([mod], "Left", lazy.screen.prev_group()),
    Key([mod], "Right", lazy.screen.next_group()),
])


for i in groups:
    keys.extend(
        [
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
        ]
    )

keys.extend([
    Key([mod], "space", lazy.layout.next()),
])

keys.extend([
  Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
  Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
  Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
  Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
  Key([mod, "control"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
  Key([mod, "control"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
  Key([mod, "control"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
  Key([mod, "control"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
  Key([mod, "shift"], "h",
      lazy.layout.grow_left(),
      lazy.layout.shrink(),
      lazy.layout.decrease_ratio(),
      lazy.layout.add(),
      ),
  Key([mod, "shift"], "l",
      lazy.layout.grow_right(),
      lazy.layout.grow(),
      lazy.layout.increase_ratio(),
      lazy.layout.delete(),
      ),
  Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
  Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
])

keys.extend([
  Key([mod, "shift"], "r", lazy.reload_config(), desc="Reload the config"),
])

keys.extend([
  Key([mod, "shift"], "d", lazy.spawn('dmenu_run')),
  Key([mod], "b", lazy.spawn('brave')),
])

@hook.subscribe.startup_once
def startup():
    autostart = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.run([autostart])
