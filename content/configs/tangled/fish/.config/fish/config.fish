set EDITOR "emacs"
set VISUAL "emacs"
set BROWSER "brave"

alias cd.ans="cd /etc/ansible"
alias cd.psline="cd ~/.config/powerline-shell/"
alias ls="exa -a"
alias ll="exa -alh"
alias l="exa -alh"
alias tree="tree -c"

alias snmpme="snmpwalk -v 2c -c public localhost"
alias eip="curl ipinfo.io/ip"
alias iip="ip addr | grep inet | grep '/24' | cut -d' ' -f6"
alias ssh="env TERM=xterm ssh"

alias emacs='fish -c /usr/bin/emacs'
alias vim="emacs --no-window-system"
alias svim="sudo emacs --no-window-system"

alias please='sudo "$BASH" -c "$(history -p !!)"'
alias gh="history | grep"
alias tldr="python3 -m tldr"
alias fresource=". ~/.config/fish/config.fish"
alias bresource=". ~/.bashrc"
alias real=". ~/.bash_aliases"

alias drun="docker run -it --rm"
alias ca="conda activate $(pwd)/.env"
alias cc="conda create -y -p .env python"

alias pacman="sudo pacman"
alias install="sudo pacman -S"

function replace
  sed "s/$argv[1]/$argv[2]/g"
end

function split
  awk -F"$argv[1]" "{print \$$argv[2]}"
end

function trash
  set filename $(echo "$argv[1]" | replace '\/' '_')
  echo $filename
  tar cvzf ~/.trash/$filename.tar $argv[1]
  rm -rf $argv[1]
end

function lt
  if test -n "$argv[1]"
      du -sh $argv[1]/*
  else
      du -sh ./*
  end
end

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
eval /opt/anaconda/bin/conda "shell.fish" "hook" $argv | source
# <<< conda initialize <<<
