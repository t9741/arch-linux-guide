#+TITLE: The Red Sky - Alacritty Configuration
#+PROPERTY: header-args :tangle "tangled/alacritty/.config/alacritty/alacritty.yml"
#+INCLUDE: ../navbar.org

* Window

Sets the padding to 3 on the x and y. This puts a small amount of space on the edges of the window.

Also, make the window 10% transparent (90% opaque). 

#+begin_src conf

  window:
    padding:
      x: 3
      y: 3
    opacity: 0.9

#+end_src
* Scrolling

Keep 50,000 lines in the terminal at one time.

#+begin_src conf

scrolling:
  history: 50000

#+end_src

* Colors

Set the terminal background and foreground colors with hex color codes.

#+begin_src conf
colors:
  primary:
   background: '#282c34'
   foreground: '#F9F9F9'
#+end_src

* Font

Set the terminal font for:

- Normal text
- Bold text
- Italic text
- Bold & italic text

#+begin_src conf
font:
  normal:
    family: DejaVu Sans Mono
    style: Book
  bold:
    family: DejaVu Sans Mono
    style: Bold
  italic:
    family: DejaVu Serif
    style: Italic
  bold_italic:
    family: DejaVu Serif
    style: Bold Italic
  size: 9.0
#+end_src
